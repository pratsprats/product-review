package com.product.review.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.product.review.entity.ErrorMessage;
import com.product.review.exception.ValidationException;

/* Class that handles ValidationException thrown from api implementation*/
@Provider
@Component
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

	public Response toResponse(ValidationException exception) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(exception.getMessage());
		errorMessage.setStatus(Status.BAD_REQUEST.getStatusCode());

		return Response.status(Status.BAD_REQUEST).entity(errorMessage).build();

	}
}
