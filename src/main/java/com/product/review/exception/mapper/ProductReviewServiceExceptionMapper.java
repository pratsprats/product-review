package com.product.review.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.product.review.entity.ErrorMessage;
import com.product.review.exception.ProductReviewServiceException;

/* Class that handles ProductReviewServiceException thrown from api implementation*/
@Provider
@Component
public class ProductReviewServiceExceptionMapper implements ExceptionMapper<ProductReviewServiceException> {

	public Response toResponse(ProductReviewServiceException exception) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(exception.getMessage());
		errorMessage.setStatus(Status.INTERNAL_SERVER_ERROR.getStatusCode());

		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorMessage).build();
	}
}
