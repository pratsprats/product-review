package com.product.review.exception;

/* Application's custom defined exception */
public class ProductReviewServiceException extends Exception {

	private static final long serialVersionUID = -6180645523793820848L;

	public ProductReviewServiceException() {
	}

	public ProductReviewServiceException(String str) {
		super(str);
	}

	public ProductReviewServiceException(Throwable cause) {
		super(cause);
	}

	public ProductReviewServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
