package com.product.review.exception;

import java.io.Serializable;

public class ValidationException extends Exception implements Serializable {
	private static final long serialVersionUID = -8167685848259468231L;

	public ValidationException() {
	}

	public ValidationException(String str) {
		super(str);
	}

	public ValidationException(Throwable cause) {
		super(cause);
	}

	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
}
