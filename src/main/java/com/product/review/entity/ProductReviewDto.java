package com.product.review.entity;

import java.io.Serializable;

public class ProductReviewDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long reviewId;
	private Long productId;
	private Long userId;
	private String userName;
	private String title;
	private String comment;
	private Integer attribute1Rating;
	private Integer attribute2Rating;
	private Integer attribute3Rating;
	private Integer attribute4Rating;
	private Integer attribute5Rating;
	private Double rating;
	private Boolean abuseFlag;
	private Boolean isVerified;
	private Integer status;

	public Long getReviewId() {
		return reviewId;
	}

	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getAttribute1Rating() {
		return attribute1Rating;
	}

	public void setAttribute1Rating(Integer attribute1Rating) {
		this.attribute1Rating = attribute1Rating;
	}

	public Integer getAttribute2Rating() {
		return attribute2Rating;
	}

	public void setAttribute2Rating(Integer attribute2Rating) {
		this.attribute2Rating = attribute2Rating;
	}

	public Integer getAttribute3Rating() {
		return attribute3Rating;
	}

	public void setAttribute3Rating(Integer attribute3Rating) {
		this.attribute3Rating = attribute3Rating;
	}

	public Integer getAttribute4Rating() {
		return attribute4Rating;
	}

	public void setAttribute4Rating(Integer attribute4Rating) {
		this.attribute4Rating = attribute4Rating;
	}

	public Integer getAttribute5Rating() {
		return attribute5Rating;
	}

	public void setAttribute5Rating(Integer attribute5Rating) {
		this.attribute5Rating = attribute5Rating;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Boolean getAbuseFlag() {
		return abuseFlag;
	}

	public void setAbuseFlag(Boolean abuseFlag) {
		this.abuseFlag = abuseFlag;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ProductReviewDto [" + (reviewId != null ? "reviewId=" + reviewId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (userId != null ? "userId=" + userId + ", " : "")
				+ (userName != null ? "userName=" + userName + ", " : "")
				+ (title != null ? "title=" + title + ", " : "") + (comment != null ? "comment=" + comment + ", " : "")
				+ (attribute1Rating != null ? "attribute1Rating=" + attribute1Rating + ", " : "")
				+ (attribute2Rating != null ? "attribute2Rating=" + attribute2Rating + ", " : "")
				+ (attribute3Rating != null ? "attribute3Rating=" + attribute3Rating + ", " : "")
				+ (attribute4Rating != null ? "attribute4Rating=" + attribute4Rating + ", " : "")
				+ (attribute5Rating != null ? "attribute5Rating=" + attribute5Rating + ", " : "")
				+ (rating != null ? "rating=" + rating + ", " : "")
				+ (abuseFlag != null ? "abuseFlag=" + abuseFlag + ", " : "")
				+ (isVerified != null ? "isVerified=" + isVerified + ", " : "")
				+ (status != null ? "status=" + status : "") + "]";
	}

}
