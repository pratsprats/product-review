package com.product.review.entity;

import java.io.Serializable;

public class ProductRatingsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long productId;
	private Double averageRating;
	private Double averageWeightedRating;
	private Integer activeReviewsCount;

	public Integer getActiveReviewsCount() {
		return activeReviewsCount;
	}

	public void setActiveReviewsCount(Integer activeReviewsCount) {
		this.activeReviewsCount = activeReviewsCount;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Double getAverageWeightedRating() {
		return averageWeightedRating;
	}

	public void setAverageWeightedRating(Double averageWeightedRating) {
		this.averageWeightedRating = averageWeightedRating;
	}

	public Double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}
}
