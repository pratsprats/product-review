package com.product.review.entity;

public enum ReviewStatus {
	INACTIVE(0), APPROVED(1), DISAPPROVED(2);
	
	private final Integer reviewStatusInt;
	
	ReviewStatus(Integer reviewStatusInt){
		this.reviewStatusInt = reviewStatusInt;
	}
	
	public Integer reviewStatusInteger(){
		return this.reviewStatusInt;
	}
	
	public String value(){
		return name();
	}
}
