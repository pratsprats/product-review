package com.product.review.cache;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.product.review.constants.Constants;

/* Wrapper class over EHCache to generate and use custom defined key */
@Component
public class CustomCache {
	private static Logger logger = LoggerFactory.getLogger(CustomCache.class);

	private Cache<String, Object> reviewCache;
	private Cache<String, Object> ratingCache;

	public CustomCache() {
		CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().build();
		cacheManager.init();

		reviewCache = cacheManager.createCache(Constants.REVIEW_CACHE_NAME, CacheConfigurationBuilder
				.newCacheConfigurationBuilder(String.class, Object.class, ResourcePoolsBuilder.heap(10)));

		ratingCache = cacheManager.createCache(Constants.RATING_CACHE_NAME, CacheConfigurationBuilder
				.newCacheConfigurationBuilder(String.class, Object.class, ResourcePoolsBuilder.heap(10)));

	}

	public Cache<String, Object> getCache(String cacheName) {
		return cacheName.equals(Constants.REVIEW_CACHE_NAME) ? reviewCache : ratingCache;
	}

	public void put(String cacheName, String key, Object value) {
		try {
			Cache<String, Object> cache = getCache(cacheName);
			if (cache != null && !key.isEmpty() && value != null) {
				cache.put(key, value);
			}
		} catch (Exception e) {
			StringBuilder errorMessage = new StringBuilder();
			errorMessage.append("Exception in put-cache : ").append(cacheName).append(", key : ").append(key);
			logger.error(errorMessage.toString(), e);
		}
	}

	public Object get(String cacheName, String key) {
		Object value = null;
		try {
			Cache<String, Object> cache = getCache(cacheName);
			if (cache != null && !key.isEmpty()) {
				value = cache.get(key);
			}
		} catch (Exception e) {
			StringBuilder errorMessage = new StringBuilder();
			errorMessage.append("Exception in get-cache : ").append(cacheName).append(", key : ").append(key);
			logger.error(errorMessage.toString(), e);
		}
		return value;
	}

	public void clearCache(String cacheName) {
		try {
			Cache<String, Object> cache = getCache(cacheName);
			if (cache != null) {
				cache.clear();
			}
		} catch (Exception e) {
			StringBuilder errorMessage = new StringBuilder();
			errorMessage.append("Exception in reload-cache : ").append(cacheName);
			logger.error(errorMessage.toString(), e);
		}
	}

	/* Create key of format - "p{productid}u{userid}" */
	public String createCacheKey(Long productId, Long userId) {
		StringBuilder keyBuilder = new StringBuilder();
		if (productId != null) {
			keyBuilder.append("p").append(productId.longValue());
		}
		if (userId != null) {
			keyBuilder.append("u").append(userId.longValue());
		}
		return keyBuilder.toString();
	}

}
