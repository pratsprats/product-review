package com.product.review.cache;

import java.util.List;

import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;

public interface ICacheManager {

	public void createReview(ProductReview review) throws ProductReviewServiceException;

	public void updateRatings(ProductRatings rating) throws ProductReviewServiceException;

	public List<ProductReviewDto> getReviewsByProductId(Long productId) throws ProductReviewServiceException;

	public List<ProductReviewDto> getReviewsByUserId(Long userId) throws ProductReviewServiceException;

	public List<ProductReviewDto> getReviewsByUserAndProductId(Long userId, Long productId)
			throws ProductReviewServiceException;

	public ProductRatingsDto getAverageWeightedRatingsByProductId(Long productId) throws ProductReviewServiceException;

	public List<Double> getAllRatingsForProduct(Long productId) throws ProductReviewServiceException;
}
