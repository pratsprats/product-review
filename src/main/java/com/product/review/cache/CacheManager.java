package com.product.review.cache;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.product.review.constants.Constants;
import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.dao.IProductReviewDao;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;
import com.product.review.objectmapper.CustomObjectMapper;

/* Cache layer between Product Review Manager and DAO layer */
@Component
public class CacheManager implements ICacheManager {
	private static Logger logger = LoggerFactory.getLogger(CacheManager.class);

	@Autowired
	private CustomCache customCache;

	@Autowired
	private IProductReviewDao productReviewDao;

	@Autowired
	private CustomObjectMapper customObjectMapper;

	@Value("${useCache}")
	private boolean useCache;

	public void createReview(ProductReview review) throws ProductReviewServiceException {
		if (useCache) {
			customCache.clearCache(Constants.REVIEW_CACHE_NAME);
		}
		productReviewDao.createReview(review);
	}

	public void updateRatings(ProductRatings rating) throws ProductReviewServiceException {
		if (useCache) {
			customCache.clearCache(Constants.RATING_CACHE_NAME);
		}
		productReviewDao.updateRatings(rating);
	}

	@SuppressWarnings("unchecked")
	public List<ProductReviewDto> getReviewsByProductId(Long productId) throws ProductReviewServiceException {
		String key = customCache.createCacheKey(productId, null);
		List<ProductReviewDto> resultantReviewsList = null;
		Object tempObject = null;
		if (useCache) {
			tempObject = customCache.get(Constants.REVIEW_CACHE_NAME, key);
			if (tempObject != null && tempObject instanceof List<?>) {
				logger.info("Returing from cache - getReviewsByProductId : " + productId);
				resultantReviewsList = (List<ProductReviewDto>) tempObject;
			}
		}
		if (resultantReviewsList == null || resultantReviewsList.isEmpty()) {
			List<ProductReview> reviewsList = productReviewDao.getReviewsByProductId(productId);
			resultantReviewsList = customObjectMapper.map(reviewsList, ProductReviewDto.class);
			if (useCache && resultantReviewsList != null && !resultantReviewsList.isEmpty()) {
				customCache.put(Constants.REVIEW_CACHE_NAME, key, resultantReviewsList);
			}
		}
		return resultantReviewsList;
	}

	@SuppressWarnings("unchecked")
	public List<ProductReviewDto> getReviewsByUserId(Long userId) throws ProductReviewServiceException {
		String key = customCache.createCacheKey(null, userId);
		List<ProductReviewDto> resultantReviewsList = null;
		Object tempObject = null;
		if (useCache) {
			tempObject = customCache.get(Constants.REVIEW_CACHE_NAME, key);
			if (tempObject != null && tempObject instanceof List<?>) {
				logger.info("Returing from cache - getReviewsByUserId : " + userId);
				resultantReviewsList = (List<ProductReviewDto>) tempObject;
			}
		}
		if (resultantReviewsList == null || resultantReviewsList.isEmpty()) {
			List<ProductReview> reviewsList = productReviewDao.getAllReviewsByUserId(userId);
			resultantReviewsList = customObjectMapper.map(reviewsList, ProductReviewDto.class);
			if (useCache && resultantReviewsList != null && !resultantReviewsList.isEmpty()) {
				customCache.put(Constants.REVIEW_CACHE_NAME, key, resultantReviewsList);
			}
		}
		return resultantReviewsList;
	}

	@SuppressWarnings("unchecked")
	public List<ProductReviewDto> getReviewsByUserAndProductId(Long userId, Long productId)
			throws ProductReviewServiceException {
		String key = customCache.createCacheKey(productId, userId);
		List<ProductReviewDto> resultantReviewsList = null;
		Object tempObject = null;
		if (useCache) {
			tempObject = customCache.get(Constants.REVIEW_CACHE_NAME, key);
			if (tempObject != null && tempObject instanceof List<?>) {
				logger.info("Returing from cache - getReviewsByUserAndProductId : " + productId + "," + userId);
				resultantReviewsList = (List<ProductReviewDto>) tempObject;
			}
		}
		if (resultantReviewsList == null || resultantReviewsList.isEmpty()) {
			List<ProductReview> reviewsList = productReviewDao.getAllReviewsByUserIdForProduct(userId, productId);
			resultantReviewsList = customObjectMapper.map(reviewsList, ProductReviewDto.class);
			if (useCache && resultantReviewsList != null && !resultantReviewsList.isEmpty()) {
				customCache.put(Constants.REVIEW_CACHE_NAME, key, resultantReviewsList);
			}
		}
		return resultantReviewsList;
	}

	public ProductRatingsDto getAverageWeightedRatingsByProductId(Long productId) throws ProductReviewServiceException {
		String key = customCache.createCacheKey(productId, null);
		ProductRatingsDto productRatings = null;
		Object tempObject = null;
		if (useCache) {
			tempObject = customCache.get(Constants.RATING_CACHE_NAME, key);
			if (tempObject != null && tempObject instanceof ProductRatingsDto) {
				logger.info("Returing from cache - getAverageWeightedRatingsByProductId : " + productId);
				productRatings = (ProductRatingsDto) tempObject;
			}
		}
		if (productRatings == null) {
			ProductRatings productRatingsDb = productReviewDao.getAverageWeightedRatingsByProductId(productId);
			if (productRatingsDb != null) {
				productRatings = customObjectMapper.map(productRatingsDb, ProductRatingsDto.class);
			}
			if (useCache && productRatings != null) {
				customCache.put(Constants.RATING_CACHE_NAME, key, productRatings);
			}
		}
		return productRatings;
	}

	/* No caching for now since this is used before update ratings */
	public List<Double> getAllRatingsForProduct(Long productId) throws ProductReviewServiceException {
		return productReviewDao.getAllRatingsForProduct(productId);
	}

}
