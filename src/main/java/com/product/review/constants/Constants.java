package com.product.review.constants;

public interface Constants {
	public static final String SERVER_ERROR = "Server error";
	public static final String SUCCESS = "Success";
	public static final String SAMPLE_REVIEW_PAYLOAD = "{\"comment\":\"this is the first test comment\",\"userId\":\"12345\","
			+ "\"productId\":\"231455\",\"userName\":\"prats\", \"title\":\"Very Good Product\", \"attribute1Rating\":\"5\", "
			+ "\"attribute2Rating\":\"2\" , \"attribute3Rating\":\"1\", \"attribute4Rating\":\"5\", \"attribute5Rating\":\"5\"}";
	public static final String SAMPLE_PRODUCT_ID = "231455";
	public static final String SAMPLE_USER_ID = "12345";
	
	public static final Integer NORMAL_MEAN = 3;
	public static final Integer NORMAL_VARIANCE = 1;

	public static final String PROPERTY_INVALID_FORMAT_ERROR_MESSAGE = "Invalid '%1$s'";

	public static final String REVIEW_CACHE_NAME = "reviewCache";
	public static final String RATING_CACHE_NAME = "ratingCache";
}
