package com.product.review.manager;

import java.util.List;

import com.product.review.jpa.entity.ProductReview;

public interface IRatingCalculator {

	public Double getAverageRatingForProduct(List<Double> ratingList);

	public Double getAverageWeightedRatingForProduct(List<Double> ratingList);

	public Double getAverageRating(ProductReview review);

}
