package com.product.review.manager;

import java.util.List;

import org.springframework.stereotype.Component;

import com.product.review.constants.Constants;
import com.product.review.jpa.entity.ProductReview;

/*Class to handle all the rating calculation logic*/
@Component
public class RatingCalculator implements IRatingCalculator {

	/* Given all the ratings, calculate Average rating for product */
	public Double getAverageRatingForProduct(List<Double> ratingList) {
		double averageRating = 0.0;

		if (ratingList != null && ratingList.size() > 0) {
			double sum = 0;
			for (Double rating : ratingList) {
				sum += rating;
			}
			averageRating = (sum / ratingList.size());
		}
		return averageRating;
	}

	/* Given all the ratings, calculate Average weighted rating for product */
	public Double getAverageWeightedRatingForProduct(List<Double> ratingList) {
		double averageWeightedRating = 0.0;
		if (ratingList != null && ratingList.size() > 0) {
			/*
			 * based on u = (x+ny)/ (v+n)
			 * 
			 * n = Total number of ratings, y = Average Rating, x = Mean for
			 * normal curve, v = Variance for Normal Curve
			 */

			int count = ratingList.size();
			if (count > 0) {
				averageWeightedRating = (Constants.NORMAL_MEAN + (getAverageRatingForProduct(ratingList)) * (count))
						/ (Constants.NORMAL_VARIANCE + count);
			}
		}
		return averageWeightedRating;
	}

	/* Given all the attributes rating, calculate overall rating for product */
	public Double getAverageRating(ProductReview review) {
		double average = 0.0;
		int counter;
		if (null != review) {
			counter = 0;
			if (review.getAttribute1Rating() != null && review.getAttribute1Rating() > 0) {
				counter++;
				average += review.getAttribute1Rating();
			}
			if (review.getAttribute2Rating() != null && review.getAttribute2Rating() > 0) {
				counter++;
				average += review.getAttribute2Rating();
			}
			if (review.getAttribute3Rating() != null && review.getAttribute3Rating() > 0) {
				counter++;
				average += review.getAttribute3Rating();
			}
			if (review.getAttribute4Rating() != null && review.getAttribute4Rating() > 0) {
				counter++;
				average += review.getAttribute4Rating();
			}
			if (review.getAttribute5Rating() != null && review.getAttribute5Rating() > 0) {
				counter++;
				average += review.getAttribute5Rating();
			}
			if (counter > 0)
				average = average / counter;
		}
		return average;
	}
}
