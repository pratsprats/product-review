package com.product.review.manager;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.product.review.cache.ICacheManager;
import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.entity.ReviewStatus;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;
import com.product.review.objectmapper.CustomObjectMapper;

/* Class that contains all the business logic for all reads and writes*/
@Component
public class ProductReviewManager implements IProductReviewManager {
	private static Logger logger = LoggerFactory.getLogger(ProductReviewManager.class);

	@Autowired
	private ICacheManager cacheManager;
	@Autowired
	private CustomObjectMapper customObjectMapper;
	@Autowired
	private IRatingCalculator ratingCalculator;

	public ProductReviewDto createReview(ProductReviewDto reviewDto) throws ProductReviewServiceException {
		ProductReview review = customObjectMapper.map(reviewDto, ProductReview.class);
		logger.info(review.toString());

		/* Set all the default values and calculate rating */
		setDefaultValues(review);

		/* Save data to product review table */
		cacheManager.createReview(review);

		/*
		 * Since Status is by default approved, calculating and updating
		 * aggregated ratings as well.
		 */
		updateRating(review.getProductId());

		return customObjectMapper.map(review, ProductReviewDto.class);
	}

	private void setDefaultValues(ProductReview review) {
		/* Calculate rating based on 5 attributes selected by user */
		if (null == review.getRating() || review.getRating() <= 0) {
			review.setRating(ratingCalculator.getAverageRating(review));
		}

		/*
		 * Verified = true if user has purchased this item in past. Defaulting
		 * to true for now
		 */
		review.setIsVerified(true); //

		/* Assuming Abuse flag to be false for now */
		review.setAbuseFlag(Boolean.FALSE);

		/* Assuming Review Status as Approved by default */
		review.setStatus(ReviewStatus.APPROVED.reviewStatusInteger());

		review.setCreatedBy(review.getUserName());
		review.setUpdatedBy(review.getUserName());
	}

	public List<ProductReviewDto> getReviewsByProductId(Long productId) throws ProductReviewServiceException {
		return cacheManager.getReviewsByProductId(productId);
	}

	public List<ProductReviewDto> getReviewsByUserId(Long userId) throws ProductReviewServiceException {
		return cacheManager.getReviewsByUserId(userId);

	}

	public List<ProductReviewDto> getReviewsByUserAndProductId(Long userId, Long productId)
			throws ProductReviewServiceException {
		return cacheManager.getReviewsByUserAndProductId(userId, productId);
	}

	public ProductRatingsDto getAverageWeightedRatingsByProductId(Long productId) throws ProductReviewServiceException {
		return cacheManager.getAverageWeightedRatingsByProductId(productId);
	}

	/* Update aggregated rating for a product */
	public void updateRating(Long productId) throws ProductReviewServiceException {
		List<Double> reviewList = cacheManager.getAllRatingsForProduct(productId);

		ProductRatings ratings = new ProductRatings();
		ratings.setProductId(productId);
		ratings.setActiveReviewsCount(reviewList.size());
		ratings.setAverageRating(ratingCalculator.getAverageRatingForProduct(reviewList));
		ratings.setAverageWeightedRating(ratingCalculator.getAverageWeightedRatingForProduct(reviewList));
		cacheManager.updateRatings(ratings);
	}

}
