package com.product.review.manager;

import java.util.List;

import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;

public interface IProductReviewManager {

	public ProductReviewDto createReview(ProductReviewDto reviewDto) throws ProductReviewServiceException;

	public List<ProductReviewDto> getReviewsByProductId(Long productId) throws ProductReviewServiceException;

	public ProductRatingsDto getAverageWeightedRatingsByProductId(Long productId) throws ProductReviewServiceException;

	public void updateRating(Long productId) throws ProductReviewServiceException;

	public List<ProductReviewDto> getReviewsByUserId(Long userId) throws ProductReviewServiceException;

	public List<ProductReviewDto> getReviewsByUserAndProductId(Long userId, Long productId)
			throws ProductReviewServiceException;
}
