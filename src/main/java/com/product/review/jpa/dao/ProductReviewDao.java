package com.product.review.jpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;

/* Class to handle all the database related operations.*/
@Repository("ProductReviewDao")
public class ProductReviewDao implements IProductReviewDao {
	private static Logger logger = LoggerFactory.getLogger(ProductReviewDao.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void createReview(ProductReview review) throws ProductReviewServiceException {
		try {
			entityManager.persist(review);
		} catch (Exception e) {
			String errorMessage = "Please try again. Some error occured while persisting new review in database ";
			logger.error(errorMessage + review, e);
			throw new ProductReviewServiceException(errorMessage);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ProductReview> getReviewsByProductId(Long productId) throws ProductReviewServiceException {

		List<ProductReview> list = null;
		try {
			Query query = entityManager.createNamedQuery(FIND_BY_PRODUCT_ID_QUERY);
			query.setParameter("productId", productId);
			list = query.getResultList();
		} catch (Exception e) {
			String errorMessage = "Please try again. Some error occured fetching reviews for product id : " + productId;
			logger.error(errorMessage, e);
			throw new ProductReviewServiceException(errorMessage);
		}
		if (null == list) {
			list = new ArrayList<ProductReview>();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Double> getAllRatingsForProduct(Long productId) throws ProductReviewServiceException {
		List<Double> list = null;
		try {
			Query query = entityManager.createNamedQuery(RATINGS_BY_PRODUCT_ID_QUERY);
			query.setParameter("productId", productId);
			list = query.getResultList();
		} catch (Exception e) {
			String errorMessage = "Please try again. Some error occured while fetching ratings for product id : "
					+ productId;
			logger.error(errorMessage, e);
			throw new ProductReviewServiceException(errorMessage);
		}
		if (null == list) {
			list = new ArrayList<Double>();
		}
		return list;
	}

	public void updateRatings(ProductRatings rating) throws ProductReviewServiceException {
		try {
			entityManager.merge(rating);
		} catch (Exception e) {
			String errorMessage = "Please try again. Some error occured while updating ratings for product  : "
					+ rating;
			logger.error(errorMessage, e);
			throw new ProductReviewServiceException(errorMessage);
		}
	}

	public ProductRatings getAverageWeightedRatingsByProductId(Long productId) throws ProductReviewServiceException {
		ProductRatings ratings = null;
		try {
			ratings = entityManager.find(ProductRatings.class, productId);
		} catch (Exception e) {
			String errorMessage = "Please try again. Some error occured while fetching aggregated ratings for product  : "
					+ productId;
			logger.error(errorMessage, e);
			throw new ProductReviewServiceException(errorMessage);
		}
		return ratings;
	}

	@SuppressWarnings("unchecked")
	public List<ProductReview> getAllReviewsByUserId(Long userId) throws ProductReviewServiceException {
		List<ProductReview> list = null;
		try {
			Query query = entityManager.createNamedQuery(FIND_BY_USER_ID_QUERY);
			query.setParameter("userId", userId);
			list = query.getResultList();
		} catch (Exception e) {
			String errorMessage = "Please try again. Some error occured while fetching reviews for user id : " + userId;
			logger.error(errorMessage, e);
			throw new ProductReviewServiceException(errorMessage);
		}
		if (null == list) {
			list = new ArrayList<ProductReview>();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<ProductReview> getAllReviewsByUserIdForProduct(Long userId, Long productId)
			throws ProductReviewServiceException {
		List<ProductReview> list = null;
		try {
			Query query = entityManager.createNamedQuery(FIND_BY_USER_AND_PRODUCT_ID_QUERY);
			query.setParameter("productId", productId);
			query.setParameter("userId", userId);
			list = query.getResultList();
		} catch (Exception e) {
			String errorMessage = "Please try again. Some error occured while fetching reviews for user id : " + userId
					+ " and product id : " + productId;
			logger.error(errorMessage, e);
			throw new ProductReviewServiceException(errorMessage);
		}
		if (null == list) {
			list = new ArrayList<ProductReview>();
		}
		return list;

	}
}
