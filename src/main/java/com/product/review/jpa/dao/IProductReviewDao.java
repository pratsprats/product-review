package com.product.review.jpa.dao;

import java.util.List;

import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;

public interface IProductReviewDao {

	public static final String FIND_BY_PRODUCT_ID_QUERY = "ProductReview.findByProductId";
	public static final String FIND_BY_USER_ID_QUERY = "ProductReview.findByUserId";
	public static final String RATINGS_BY_PRODUCT_ID_QUERY = "ProductReview.ratingsByProductId";
	public static final String FIND_BY_USER_AND_PRODUCT_ID_QUERY = "ProductReview.findByUserAndProductId";

	public void createReview(ProductReview review) throws ProductReviewServiceException;

	public void updateRatings(ProductRatings rating) throws ProductReviewServiceException;

	public List<ProductReview> getReviewsByProductId(Long productId) throws ProductReviewServiceException;

	public ProductRatings getAverageWeightedRatingsByProductId(Long productId) throws ProductReviewServiceException;

	public List<Double> getAllRatingsForProduct(Long productId) throws ProductReviewServiceException;

	public List<ProductReview> getAllReviewsByUserIdForProduct(Long userId, Long productId)
			throws ProductReviewServiceException;

	public List<ProductReview> getAllReviewsByUserId(Long userId) throws ProductReviewServiceException;

}
