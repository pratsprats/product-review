package com.product.review.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "product_ratings")
@DynamicInsert
@DynamicUpdate
public class ProductRatings extends GenericEntityBase implements Serializable {

	private static final long serialVersionUID = -8591541420959245392L;

	@Id
	@Column(name = "product_id")
	private Long productId;

	@Column(name = "average_weighted_rating")
	private Double averageWeightedRating;

	@Column(name = "average_rating")
	private Double averageRating;

	@Column(name = "active_reviews_count")
	private Integer activeReviewsCount;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Double getAverageWeightedRating() {
		return averageWeightedRating;
	}

	public void setAverageWeightedRating(Double averageWeightedRating) {
		this.averageWeightedRating = averageWeightedRating;
	}

	public Double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	public Integer getActiveReviewsCount() {
		return activeReviewsCount;
	}

	public void setActiveReviewsCount(Integer activeReviewsCount) {
		this.activeReviewsCount = activeReviewsCount;
	}

	@Override
	public String toString() {
		return "ProductRatings [" + (productId != null ? "productId=" + productId + ", " : "")
				+ (averageWeightedRating != null ? "averageWeightedRating=" + averageWeightedRating + ", " : "")
				+ (averageRating != null ? "averageRating=" + averageRating + ", " : "")
				+ (activeReviewsCount != null ? "activeReviewsCount=" + activeReviewsCount : "") + "]";
	}

}
