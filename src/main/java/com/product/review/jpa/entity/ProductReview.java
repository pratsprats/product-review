package com.product.review.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.product.review.jpa.dao.IProductReviewDao;

@Entity
@Table(name = "product_review")
@DynamicInsert
@DynamicUpdate
@NamedQueries({
		@NamedQuery(name = IProductReviewDao.FIND_BY_PRODUCT_ID_QUERY, query = "Select r from ProductReview r where r.productId=:productId and r.status=1 order by r.createDate desc"),
		@NamedQuery(name = IProductReviewDao.FIND_BY_USER_ID_QUERY, query = "Select r from ProductReview r where r.userId=:userId and r.status=1 order by r.createDate desc"),
		@NamedQuery(name = IProductReviewDao.FIND_BY_USER_AND_PRODUCT_ID_QUERY, query = "Select r from ProductReview r where r.userId=:userId and r.productId=:productId and r.status=1 order by r.createDate desc"),
		@NamedQuery(name = IProductReviewDao.RATINGS_BY_PRODUCT_ID_QUERY, query = "Select r.rating from ProductReview r where r.productId=:productId and r.status=1 and r.rating is not null and r.rating>0"), })
public class ProductReview extends GenericEntityBase implements Serializable {

	private static final long serialVersionUID = 414216576598137726L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	private Long reviewId;

	@Column(name = "product_id")
	private Long productId;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "title")
	private String title;

	@Column(name = "comment")
	private String comment;

	@Column(name = "rating")
	private Double rating;

	@Column(name = "attribute1_rating")
	private Integer attribute1Rating;

	@Column(name = "attribute2_rating")
	private Integer attribute2Rating;

	@Column(name = "attribute3_rating")
	private Integer attribute3Rating;

	@Column(name = "attribute4_rating")
	private Integer attribute4Rating;

	@Column(name = "attribute5_rating")
	private Integer attribute5Rating;

	@Column(name = "abuse_flag")
	private Boolean abuseFlag;

	@Column(name = "is_verified ")
	private Boolean isVerified;

	/*
	 * 0 = Initially, 1 = approved 2 = disapproved
	 */
	@Column(name = "status")
	private Integer status;

	public Long getReviewId() {
		return reviewId;
	}

	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Integer getAttribute1Rating() {
		return attribute1Rating;
	}

	public void setAttribute1Rating(Integer attribute1Rating) {
		this.attribute1Rating = attribute1Rating;
	}

	public Integer getAttribute2Rating() {
		return attribute2Rating;
	}

	public void setAttribute2Rating(Integer attribute2Rating) {
		this.attribute2Rating = attribute2Rating;
	}

	public Integer getAttribute3Rating() {
		return attribute3Rating;
	}

	public void setAttribute3Rating(Integer attribute3Rating) {
		this.attribute3Rating = attribute3Rating;
	}

	public Integer getAttribute4Rating() {
		return attribute4Rating;
	}

	public void setAttribute4Rating(Integer attribute4Rating) {
		this.attribute4Rating = attribute4Rating;
	}

	public Integer getAttribute5Rating() {
		return attribute5Rating;
	}

	public void setAttribute5Rating(Integer attribute5Rating) {
		this.attribute5Rating = attribute5Rating;
	}

	public Boolean getAbuseFlag() {
		return abuseFlag;
	}

	public void setAbuseFlag(Boolean abuseFlag) {
		this.abuseFlag = abuseFlag;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String toString() {
		return "ProductReview [" + (reviewId != null ? "reviewId=" + reviewId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (userId != null ? "userId=" + userId + ", " : "")
				+ (userName != null ? "userName=" + userName + ", " : "")
				+ (title != null ? "title=" + title + ", " : "") + (comment != null ? "comment=" + comment + ", " : "")
				+ (rating != null ? "rating=" + rating + ", " : "")
				+ (attribute1Rating != null ? "attribute1Rating=" + attribute1Rating + ", " : "")
				+ (attribute2Rating != null ? "attribute2Rating=" + attribute2Rating + ", " : "")
				+ (attribute3Rating != null ? "attribute3Rating=" + attribute3Rating + ", " : "")
				+ (attribute4Rating != null ? "attribute4Rating=" + attribute4Rating + ", " : "")
				+ (attribute5Rating != null ? "attribute5Rating=" + attribute5Rating + ", " : "")
				+ (abuseFlag != null ? "abuseFlag=" + abuseFlag + ", " : "")
				+ (isVerified != null ? "isVerified=" + isVerified + ", " : "")
				+ (status != null ? "status=" + status : "") + "]";
	}

}
