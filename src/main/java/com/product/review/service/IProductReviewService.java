package com.product.review.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.product.review.constants.Constants;
import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.exception.ValidationException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/v1")
@Api(value = "/v1", description = "All api's related to product review and ratings")
public interface IProductReviewService {

	@POST
	@Path("/review")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ApiOperation(value = "Submit a new review for a particular product")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS),
			@ApiResponse(code = 500, message = Constants.SERVER_ERROR) })
	public ProductReviewDto createReview(
			@ApiParam(defaultValue = Constants.SAMPLE_REVIEW_PAYLOAD, required = true) ProductReviewDto reviewDto)
			throws ProductReviewServiceException, ValidationException;

	@GET
	@Path("/review/product/{productId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ApiOperation(value = "Get all reviews for a particular product")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS),
			@ApiResponse(code = 500, message = Constants.SERVER_ERROR) })
	public List<ProductReviewDto> getReviewsByProductId(
			@ApiParam(defaultValue = Constants.SAMPLE_PRODUCT_ID, required = true) @PathParam("productId") Long productId)
			throws ProductReviewServiceException, ValidationException;

	@GET
	@Path("/review/user/{userId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ApiOperation(value = "Get all reviews for a particular user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS),
			@ApiResponse(code = 500, message = Constants.SERVER_ERROR) })
	public List<ProductReviewDto> getReviewsByUserId(
			@ApiParam(defaultValue = Constants.SAMPLE_USER_ID, required = true) @PathParam("userId") Long userId)
			throws ProductReviewServiceException, ValidationException;

	@GET
	@Path("/review/product/{productId}/user/{userId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ApiOperation(value = "Get all reviews for a particular product and user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS),
			@ApiResponse(code = 500, message = Constants.SERVER_ERROR) })
	public List<ProductReviewDto> getReviewsByUserAndProductId(
			@ApiParam(defaultValue = Constants.SAMPLE_PRODUCT_ID, required = true) @PathParam("productId") Long productId,
			@ApiParam(defaultValue = Constants.SAMPLE_USER_ID, required = true) @PathParam("userId") Long userId)
			throws ProductReviewServiceException, ValidationException;

	@GET
	@Path("/rating/product/{productId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ApiOperation(value = "Get aggregated rating for a particular product")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS),
			@ApiResponse(code = 500, message = Constants.SERVER_ERROR) })
	public ProductRatingsDto getProductRating(
			@ApiParam(defaultValue = Constants.SAMPLE_PRODUCT_ID, required = true) @PathParam("productId") Long productId)
			throws ProductReviewServiceException, ValidationException;

}
