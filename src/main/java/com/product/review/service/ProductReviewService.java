package com.product.review.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.exception.ValidationException;
import com.product.review.manager.IProductReviewManager;
import com.product.review.validator.ProductReviewApiValidator;

/* Implementation class for all the rest api's exposed to end user*/
@Component
public class ProductReviewService implements IProductReviewService {
	private static Logger logger = LoggerFactory.getLogger(ProductReviewService.class);

	@Autowired
	private IProductReviewManager productReviewManager;

	@Transactional(readOnly = false)
	public ProductReviewDto createReview(ProductReviewDto reviewDto)
			throws ProductReviewServiceException, ValidationException {
		try {
			ProductReviewApiValidator.validateReviewDto(reviewDto);
			logger.info(reviewDto.toString());
			return productReviewManager.createReview(reviewDto);
		} catch (ProductReviewServiceException e) {
			throw e;
		} catch (ValidationException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Some Error occurred while creating new review : " + reviewDto, e);
			throw new ProductReviewServiceException(e.getMessage(), e);
		}
	}

	@Transactional(readOnly = true)
	public List<ProductReviewDto> getReviewsByProductId(Long productId)
			throws ProductReviewServiceException, ValidationException {
		try {
			ProductReviewApiValidator.validateField("productId", productId);
			return productReviewManager.getReviewsByProductId(productId);
		} catch (ProductReviewServiceException e) {
			throw e;
		} catch (ValidationException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Some Error occurred while fetching reviews for product : " + productId, e);
			throw new ProductReviewServiceException(e.getMessage(), e);
		}
	}

	@Transactional(readOnly = true)
	public List<ProductReviewDto> getReviewsByUserId(Long userId)
			throws ProductReviewServiceException, ValidationException {
		try {
			ProductReviewApiValidator.validateField("userId", userId);
			return productReviewManager.getReviewsByUserId(userId);
		} catch (ProductReviewServiceException e) {
			throw e;
		} catch (ValidationException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Some Error occurred while fetching reviews for user : " + userId, e);
			throw new ProductReviewServiceException(e.getMessage(), e);
		}
	}

	@Transactional(readOnly = true)
	public List<ProductReviewDto> getReviewsByUserAndProductId(Long productId, Long userId)
			throws ProductReviewServiceException, ValidationException {
		try {
			ProductReviewApiValidator.validateField("productId", productId);
			ProductReviewApiValidator.validateField("userId", userId);
			return productReviewManager.getReviewsByUserAndProductId(userId, productId);
		} catch (ProductReviewServiceException e) {
			throw e;
		} catch (ValidationException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Some Error occurred while fetching reviews for product : " + productId, e);
			throw new ProductReviewServiceException(e.getMessage(), e);
		}
	}

	@Transactional(readOnly = true)
	public ProductRatingsDto getProductRating(Long productId)
			throws ProductReviewServiceException, ValidationException {
		try {
			ProductReviewApiValidator.validateField("productId", productId);
			return productReviewManager.getAverageWeightedRatingsByProductId(productId);
		} catch (ProductReviewServiceException e) {
			throw e;
		} catch (ValidationException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Some Error occurred while fetching rating for product : " + productId, e);
			throw new ProductReviewServiceException(e.getMessage(), e);
		}
	}

}
