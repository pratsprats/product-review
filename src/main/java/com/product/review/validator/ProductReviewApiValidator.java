package com.product.review.validator;

import java.util.Formatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.product.review.constants.Constants;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ValidationException;

/* Class to handle all the input validations */
public class ProductReviewApiValidator {
	private static Logger logger = LoggerFactory.getLogger(ProductReviewApiValidator.class);

	public static void validateReviewDto(ProductReviewDto reviewDto) throws ValidationException {
		ProductReviewApiValidator.validateField("reviewDto", reviewDto);
		ProductReviewApiValidator.validateField("productId", reviewDto.getProductId());
		ProductReviewApiValidator.validateField("userId", reviewDto.getUserId());
		ProductReviewApiValidator.validateField("userName", reviewDto.getUserName());
		ProductReviewApiValidator.validateField("title", reviewDto.getTitle());
		ProductReviewApiValidator.validateField("comment", reviewDto.getComment());
		ProductReviewApiValidator.validateField("attribute1Rating", reviewDto.getAttribute1Rating());
		ProductReviewApiValidator.validateField("attribute2Rating", reviewDto.getAttribute2Rating());
		ProductReviewApiValidator.validateField("attribute3Rating", reviewDto.getAttribute3Rating());
		ProductReviewApiValidator.validateField("attribute4Rating", reviewDto.getAttribute4Rating());
		ProductReviewApiValidator.validateField("attribute5Rating", reviewDto.getAttribute5Rating());
	}

	public static void validateField(String propertyName, Object propertyValue) throws ValidationException {
		if (propertyValue == null) {
			createErrorMessage(propertyName);
		}
	}

	public static void validateField(String propertyName, Long propertyValue) throws ValidationException {
		if (propertyValue == null || propertyValue <= 0) {
			createErrorMessage(propertyName);
		}
	}

	public static void validateField(String propertyName, Integer propertyValue) throws ValidationException {
		/* Rating should be between 1-5 only */
		if (propertyValue == null || propertyValue <= 0 || propertyValue > 5) {
			createErrorMessage(propertyName);
		}
	}

	private static void createErrorMessage(String propertyName) throws ValidationException {
		Formatter formatter = new Formatter();
		String errorStr = formatter.format(Constants.PROPERTY_INVALID_FORMAT_ERROR_MESSAGE, propertyName).toString();
		formatter.close();
		logger.error(errorStr);
		throw new ValidationException(errorStr);
	}
}
