package com.product.review.objectmapper;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/* Wrapper over Dozer mapper to handle java.util.list mappings */
@Component
public class CustomObjectMapper {

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	public <T, U> U map(final T source, final Class<U> destType) {
		return dozerBeanMapper.map(source, destType);
	}

	public <T, U> List<U> map(final List<T> source, final Class<U> destType) {
		final List<U> dest = new ArrayList<U>();
		for (T element : source) {
			dest.add(dozerBeanMapper.map(element, destType));
		}
		return dest;
	}

}
