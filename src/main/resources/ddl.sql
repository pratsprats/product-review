DROP TABLE IF EXISTS product_review;
CREATE TABLE product_review (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  product_id bigint(20) NOT NULL,
  user_id bigint(20) NOT NULL,
  user_name varchar(20) NOT NULL,
  title varchar(50) NOT NULL,
  comment text NOT NULL,
  rating double(5,1) NOT NULL,
  attribute1_rating tinyint(1) NOT NULL,
  attribute2_rating tinyint(1) NOT NULL,
  attribute3_rating tinyint(1) NOT NULL,
  attribute4_rating tinyint(1) NOT NULL,
  attribute5_rating tinyint(1) NOT NULL,
  abuse_flag tinyint(1) NOT NULL DEFAULT 0,
  is_verified tinyint(1) NOT NULL,
  status tinyint(1) NOT NULL DEFAULT 0,
  create_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_by varchar(20),
  updated_by varchar(20),
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS product_ratings;
CREATE TABLE product_ratings (
  product_id bigint(20) NOT NULL,
  active_reviews_count int NOT NULL,
  average_weighted_rating double(5,1) NOT NULL,
  average_rating double(5,1) NOT NULL,
  create_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_by varchar(20),
  updated_by varchar(20),
  PRIMARY KEY (product_id)
);