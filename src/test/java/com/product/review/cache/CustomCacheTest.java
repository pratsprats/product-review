package com.product.review.cache;

import org.testng.annotations.Test;

import junit.framework.Assert;
import mockit.Tested;

public class CustomCacheTest {

	@Tested
	private CustomCache cache;

	@Test
	public void putTest() {
		cache.put("ratingCache", "myKey", "anyValue");
	}

	@Test
	public void getTest() {
		cache.put("ratingCache", "myKey", "anyValue");
		Object value = cache.get("ratingCache", "myKey");
		Assert.assertEquals(value, "anyValue");
		cache.clearCache("ratingCache");
		value = cache.get("ratingCache", "myKey");
		Assert.assertNull(value);

	}

	@Test
	public void createCacheKeyTest() {
		String key = cache.createCacheKey(1l, 2l);
		Assert.assertEquals(key, "p1u2");
	}

	@Test
	public void createCacheKeyTest1() {
		String key = cache.createCacheKey(1l, null);
		Assert.assertEquals(key, "p1");
	}

	@Test
	public void createCacheKeyTest2() {
		String key = cache.createCacheKey(null, 2l);
		Assert.assertEquals(key, "u2");
	}

}
