package com.product.review.cache;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.dao.IProductReviewDao;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;
import com.product.review.objectmapper.CustomObjectMapper;

import mockit.Injectable;
import mockit.StrictExpectations;
import mockit.Tested;

public class CacheManagerTest {

	@Tested
	private CacheManager cacheManager;

	@Injectable
	private CustomCache customCache;

	@Injectable
	private IProductReviewDao productReviewDao;

	@Injectable
	private CustomObjectMapper customObjectMapper;

	@Injectable(value = "true")
	private boolean useCache;

	@Test
	public void createReviewTest() {
		final ProductReview review = new ProductReview();
		review.setProductId(11L);

		try {
			new StrictExpectations() {
				{
					customCache.clearCache(anyString);
					minTimes = 1;

					productReviewDao.createReview(review);
					minTimes = 1;

				}
			};
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}

		try {
			cacheManager.createReview(review);
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void updateRatingsTest() {
		final ProductRatings ratings = new ProductRatings();

		try {
			new StrictExpectations() {
				{
					customCache.clearCache(anyString);
					minTimes = 1;

					productReviewDao.updateRatings(ratings);
					minTimes = 1;

				}
			};
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}

		try {
			cacheManager.updateRatings(ratings);
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getReviewsByProductIdWithCacheTest() {
		final List<ProductReviewDto> list = new ArrayList<ProductReviewDto>();

		ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");
		list.add(reviewdto);

		final List<ProductReview> list1 = new ArrayList<ProductReview>();

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = list;

					productReviewDao.getReviewsByProductId(anyLong);
					minTimes = 0;
					result = list1;

					customObjectMapper.map((List) any, ProductReviewDto.class);
					minTimes = 1;
					result = list;

					customCache.put(anyString, anyString, (List) any);
					minTimes = 0;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			List<ProductReviewDto> response = cacheManager.getReviewsByProductId(123l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.get(0).getUserName(), "testUser");
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getReviewsByProductIdWithoutCacheTest() {
		final List<ProductReviewDto> list = new ArrayList<ProductReviewDto>();

		ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");
		list.add(reviewdto);

		final List<ProductReview> list1 = new ArrayList<ProductReview>();

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = null;

					productReviewDao.getReviewsByProductId(anyLong);
					minTimes = 1;
					result = list1;

					customObjectMapper.map((List) any, ProductReviewDto.class);
					minTimes = 1;
					result = list;

					customCache.put(anyString, anyString, (List) any);
					minTimes = 1;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			List<ProductReviewDto> response = cacheManager.getReviewsByProductId(123l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.get(0).getUserName(), "testUser");
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getReviewsByUserIdWithCacheTest() {
		final List<ProductReviewDto> list = new ArrayList<ProductReviewDto>();

		ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");
		list.add(reviewdto);

		final List<ProductReview> list1 = new ArrayList<ProductReview>();

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = list;

					productReviewDao.getAllReviewsByUserId(anyLong);
					minTimes = 0;
					result = list1;

					customObjectMapper.map((List) any, ProductReviewDto.class);
					minTimes = 1;
					result = list;

					customCache.put(anyString, anyString, (List) any);
					minTimes = 0;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			List<ProductReviewDto> response = cacheManager.getReviewsByUserId(123l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.get(0).getUserName(), "testUser");
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getReviewsByUserIdWithoutCacheTest() {
		final List<ProductReviewDto> list = new ArrayList<ProductReviewDto>();

		ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");
		list.add(reviewdto);

		final List<ProductReview> list1 = new ArrayList<ProductReview>();

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = null;

					productReviewDao.getAllReviewsByUserId(anyLong);
					minTimes = 1;
					result = list1;

					customObjectMapper.map((List) any, ProductReviewDto.class);
					minTimes = 1;
					result = list;

					customCache.put(anyString, anyString, (List) any);
					minTimes = 1;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			List<ProductReviewDto> response = cacheManager.getReviewsByUserId(123l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.get(0).getUserName(), "testUser");
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getReviewsByUserIdAndProductIdWithCacheTest() {
		final List<ProductReviewDto> list = new ArrayList<ProductReviewDto>();

		ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");
		list.add(reviewdto);

		final List<ProductReview> list1 = new ArrayList<ProductReview>();

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = list;

					productReviewDao.getAllReviewsByUserIdForProduct(anyLong, anyLong);
					minTimes = 0;
					result = list1;

					customObjectMapper.map((List) any, ProductReviewDto.class);
					minTimes = 1;
					result = list;

					customCache.put(anyString, anyString, (List) any);
					minTimes = 0;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			List<ProductReviewDto> response = cacheManager.getReviewsByUserAndProductId(123l, 234l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.get(0).getUserName(), "testUser");
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getReviewsByUserIdAndProductIdWithoutCacheTest() {
		final List<ProductReviewDto> list = new ArrayList<ProductReviewDto>();

		ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");
		list.add(reviewdto);

		final List<ProductReview> list1 = new ArrayList<ProductReview>();

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = null;

					productReviewDao.getAllReviewsByUserIdForProduct(anyLong, anyLong);
					minTimes = 1;
					result = list1;

					customObjectMapper.map((List) any, ProductReviewDto.class);
					minTimes = 1;
					result = list;

					customCache.put(anyString, anyString, (List) any);
					minTimes = 1;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			List<ProductReviewDto> response = cacheManager.getReviewsByUserAndProductId(123l, 234l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.get(0).getUserName(), "testUser");
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getAverageWeightedRatingsByProductIdWithCacheTest() {

		final ProductRatingsDto reviewdto = new ProductRatingsDto();
		reviewdto.setProductId(123l);
		reviewdto.setActiveReviewsCount(30);

		final ProductRatings productReview = new ProductRatings();
		productReview.setProductId(123l);
		productReview.setActiveReviewsCount(30);

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = reviewdto;

					productReviewDao.getAverageWeightedRatingsByProductId(anyLong);
					minTimes = 0;
					result = productReview;

					customObjectMapper.map(productReview, ProductRatingsDto.class);
					minTimes = 1;
					result = reviewdto;

					customCache.put(anyString, anyString, reviewdto);
					minTimes = 0;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			ProductRatingsDto response = cacheManager.getAverageWeightedRatingsByProductId(234l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.getActiveReviewsCount().intValue(), 30);
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void getAverageWeightedRatingsByProductIdWithoutCacheTest() {

		final ProductRatingsDto reviewdto = new ProductRatingsDto();
		reviewdto.setProductId(123l);
		reviewdto.setActiveReviewsCount(30);

		final ProductRatings productReview = new ProductRatings();
		productReview.setProductId(123l);
		productReview.setActiveReviewsCount(30);

		try {
			new StrictExpectations() {
				{
					customCache.createCacheKey(anyLong, anyLong);
					minTimes = 1;
					result = "key";

					customCache.get(anyString, anyString);
					minTimes = 1;
					result = null;

					productReviewDao.getAverageWeightedRatingsByProductId(anyLong);
					minTimes = 1;
					result = productReview;

					customObjectMapper.map(productReview, ProductRatingsDto.class);
					minTimes = 1;
					result = reviewdto;

					customCache.put(anyString, anyString, reviewdto);
					minTimes = 1;

				}
			};
		} catch (ProductReviewServiceException e1) {
			Assert.fail();
		}

		try {
			ProductRatingsDto response = cacheManager.getAverageWeightedRatingsByProductId(234l);
			Assert.assertNotNull(response);
			Assert.assertEquals(response.getActiveReviewsCount().intValue(), 30);
		} catch (ProductReviewServiceException e) {
			Assert.fail();
		}
	}

}
