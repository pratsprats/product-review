package com.product.review.manager;

import mockit.Injectable;
import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.Tested;
import org.slf4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.product.review.jpa.entity.ProductReview;
import com.product.review.manager.RatingCalculator;

import java.util.LinkedList;
import java.util.List;

public class RatingCalculatorTest {

	@Mocked(stubOutClassInitialization = true)
	private Logger logger;

	@Tested
	private RatingCalculator ratingCalculator;

	@Injectable
	private ProductReview productReview;

	@Test
	public void testGetAverageRatingForProduct_positiveRating() {
		List<Double> ratingList = new LinkedList<Double>();
		ratingList.add(1.0);
		ratingList.add(3.5);

		Double response = ratingCalculator.getAverageRatingForProduct(ratingList);
		Assert.assertEquals(2.25, response); // Average of 1.0 + 3.5 is 2.25
	}

	@Test
	public void testGetAverageRatingForProduct_emptyRatingList() {
		List<Double> ratingList = new LinkedList<Double>();

		Double response = ratingCalculator.getAverageRatingForProduct(ratingList);
		Assert.assertEquals(0.0, response);
	}

	@Test
	public void testGetAverageRatingForProduct_nullRatingList() {
		List<Double> ratingList = null;

		Double response = ratingCalculator.getAverageRatingForProduct(ratingList);
		Assert.assertEquals(0.0, response);
	}

	@Test
	public void testGetAverageWeightedRatingForProduct_positiveRating() {
		List<Double> ratingList = new LinkedList<Double>();
		ratingList.add(1.0);
		ratingList.add(3.5);

		Double response = ratingCalculator.getAverageWeightedRatingForProduct(ratingList);
		// based on u = (x+ny)/ (v+n)
		//
		// Where-
		// n = Total number of ratings
		// Y = Average Rating
		// x = Mean for Normal Curve
		// v = Variance for Normal Curve
		//
		// Expected of 1.0 and 3.5 would be 2.5
		Assert.assertEquals(2.5, response);
	}

	@Test
	public void testGetAverageWeightedRatingForProduct_emptyRatingList() {
		List<Double> ratingList = new LinkedList<Double>();

		Double response = ratingCalculator.getAverageWeightedRatingForProduct(ratingList);
		Assert.assertEquals(0.0, response);
	}

	@Test
	public void testGetAverageWeightedRatingForProduct_nullRatingList() {
		List<Double> ratingList = null;

		Double response = ratingCalculator.getAverageWeightedRatingForProduct(ratingList);
		Assert.assertEquals(0.0, response);
	}

	@Test
	public void testGetAverageRating_positiveRating() {
		new StrictExpectations() {
			{
				productReview.getAttribute1Rating();
				minTimes = 1;
				result = 1;
				productReview.getAttribute2Rating();
				minTimes = 1;
				result = 1;
				productReview.getAttribute3Rating();
				minTimes = 1;
				result = 1;
				productReview.getAttribute4Rating();
				minTimes = 1;
				result = 1;
				productReview.getAttribute5Rating();
				minTimes = 1;
				result = 1;
			}
		};

		Double response = ratingCalculator.getAverageRating(productReview);
		Assert.assertEquals(1.0, response); // Average of 5 reviews with rating
											// 1 would be 1.0
	}

	@Test
	public void testGetAverageRating_nullRating() {
		Double response = ratingCalculator.getAverageRating(null);
		Assert.assertEquals(0.0, response);
	}
}
