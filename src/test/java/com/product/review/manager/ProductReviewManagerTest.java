package com.product.review.manager;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.product.review.cache.ICacheManager;
import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;
import com.product.review.objectmapper.CustomObjectMapper;

import mockit.Deencapsulation;
import mockit.Injectable;
import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.Tested;

public class ProductReviewManagerTest {

	@Mocked(stubOutClassInitialization = true)
	private Logger logger;

	@Tested
	private ProductReviewManager productReviewManager;

	@Injectable
	private ICacheManager cacheManager;

	@Injectable
	private CustomObjectMapper customObjectMapper;

	@Injectable
	private IRatingCalculator ratingCalculator;

	@Test
	public void testCreateReview_notNull() throws Exception {
		final ProductReviewDto reviewDto = new ProductReviewDto();
		final ProductReview review = new ProductReview();
		review.setProductId(11L);
		final ProductReviewDto productReviewDto = new ProductReviewDto();
		final List<Double> reviewList = new LinkedList<Double>();

		new StrictExpectations() {
			{
				customObjectMapper.map(reviewDto, ProductReview.class);
				minTimes = 1;
				result = review;

				Deencapsulation.invoke(productReviewManager, "setDefaultValues", review);
				minTimes = 1;

				ratingCalculator.getAverageRating(review);
				minTimes = 1;
				result = 11L;

				cacheManager.createReview(review);
				minTimes = 1;

				productReviewManager.updateRating(11L);
				minTimes = 1;

				cacheManager.getAllRatingsForProduct(11L);
				minTimes = 1;
				result = reviewList;

				ratingCalculator.getAverageRatingForProduct(reviewList);
				minTimes = 1;
				result = 11L;

				ratingCalculator.getAverageWeightedRatingForProduct(reviewList);
				minTimes = 1;
				result = 11L;

				cacheManager.updateRatings((ProductRatings) any);
				minTimes = 1;

				customObjectMapper.map(review, ProductReviewDto.class);
				minTimes = 1;
				result = productReviewDto;
			}
		};

		ProductReviewDto response = productReviewManager.createReview(reviewDto);
		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAverageWeightedRatingsByProductId_notNull() throws Exception {
		final ProductRatingsDto productRatingsDto = new ProductRatingsDto();
		new StrictExpectations() {
			{
				cacheManager.getAverageWeightedRatingsByProductId(11L);
				minTimes = 1;
				result = productRatingsDto;
			}
		};

		ProductRatingsDto response = productReviewManager.getAverageWeightedRatingsByProductId(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetAverageWeightedRatingsByProductId_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				cacheManager.getAverageWeightedRatingsByProductId(11L);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewManager.getAverageWeightedRatingsByProductId(11L);
	}

	@Test
	public void testUpdateRating_notNull() throws Exception {
		final List<Double> reviewList = new LinkedList<Double>();

		new StrictExpectations() {
			{
				cacheManager.getAllRatingsForProduct(11L);
				minTimes = 1;
				result = reviewList;

				ratingCalculator.getAverageRatingForProduct(reviewList);
				minTimes = 1;
				result = 11L;

				ratingCalculator.getAverageWeightedRatingForProduct(reviewList);
				result = 11L;

				cacheManager.updateRatings((ProductRatings) any);
				minTimes = 1;
			}
		};

		productReviewManager.updateRating(11L);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testUpdateRating_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();

		new StrictExpectations() {
			{
				cacheManager.getAllRatingsForProduct(11L);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewManager.updateRating(11L);
	}
}
