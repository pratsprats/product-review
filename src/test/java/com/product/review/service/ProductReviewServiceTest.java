package com.product.review.service;

import com.product.review.entity.ProductRatingsDto;
import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ProductReviewServiceException;
import com.product.review.exception.ValidationException;
import com.product.review.manager.IProductReviewManager;
import com.product.review.service.ProductReviewService;
import com.product.review.validator.ProductReviewApiValidator;

import mockit.*;
import org.slf4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

public class ProductReviewServiceTest {

	@Mocked(stubOutClassInitialization = true)
	private Logger logger;

	@Tested
	private ProductReviewService productReviewService;

	@Mocked
	private ProductReviewApiValidator productReviewApiValidator;

	@Injectable
	private IProductReviewManager productReviewManager;

	@Test
	public void testCreateReview_notNull() throws Exception {
		final ProductReviewDto reviewDto = new ProductReviewDto();
		final ProductReviewDto productReviewDto = new ProductReviewDto();
		new StrictExpectations() {
			{
				productReviewApiValidator.validateReviewDto(reviewDto);
				minTimes = 1;

				productReviewManager.createReview(reviewDto);
				minTimes = 1;
				result = productReviewDto;
			}
		};

		ProductReviewDto response = productReviewService.createReview(reviewDto);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ValidationException.class)
	public void testCreateReview_throwValidationException() throws Exception {
		final ProductReviewDto reviewDto = new ProductReviewDto();
		final ValidationException validationException = new ValidationException();
		new StrictExpectations() {
			{
				productReviewApiValidator.validateReviewDto(reviewDto);
				minTimes = 1;
				result = validationException;
			}
		};

		productReviewService.createReview(reviewDto);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testCreateReview_throwProductReviewServiceException() throws Exception {
		final ProductReviewDto reviewDto = new ProductReviewDto();
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				productReviewApiValidator.validateReviewDto(reviewDto);
				minTimes = 1;

				productReviewManager.createReview(reviewDto);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewService.createReview(reviewDto);
	}

	@Test
	public void testGetReviewsByProductId_notNull() throws Exception {
		final List<ProductReviewDto> productReviewDtoList = new LinkedList<ProductReviewDto>();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;

				productReviewManager.getReviewsByProductId(anyLong);
				minTimes = 1;
				result = productReviewDtoList;
			}
		};

		List<ProductReviewDto> response = productReviewService.getReviewsByProductId(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ValidationException.class)
	public void testGetReviewsByProductId_throwValidationException() throws Exception {
		final ValidationException validationException = new ValidationException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;
				result = validationException;
			}
		};

		productReviewService.getReviewsByProductId(11L);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetReviewsByProductId_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;

				productReviewManager.getReviewsByProductId(anyLong);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewService.getReviewsByProductId(11L);
	}

	@Test
	public void testGetReviewsByUserId_notNull() throws Exception {
		final List<ProductReviewDto> productReviewDtoList = new LinkedList<ProductReviewDto>();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("userId", anyLong);
				minTimes = 1;

				productReviewManager.getReviewsByUserId(anyLong);
				minTimes = 1;
				result = productReviewDtoList;
			}
		};

		List<ProductReviewDto> response = productReviewService.getReviewsByUserId(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ValidationException.class)
	public void testGetReviewsByUserId_throwValidationException() throws Exception {
		final ValidationException validationException = new ValidationException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("userId", anyLong);
				minTimes = 1;
				result = validationException;
			}
		};

		productReviewService.getReviewsByUserId(11L);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetReviewsByUserId_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("userId", anyLong);
				minTimes = 1;

				productReviewManager.getReviewsByUserId(anyLong);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewService.getReviewsByUserId(11L);
	}

	@Test
	public void testGetReviewsByUserAndProductId_notNull() throws Exception {
		final List<ProductReviewDto> productReviewDtoList = new LinkedList<ProductReviewDto>();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;

				ProductReviewApiValidator.validateField("userId", anyLong);
				minTimes = 1;

				productReviewManager.getReviewsByUserAndProductId(anyLong, anyLong);
				minTimes = 1;
				result = productReviewDtoList;
			}
		};

		List<ProductReviewDto> response = productReviewService.getReviewsByUserAndProductId(11L, 10L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ValidationException.class)
	public void testGetReviewsByUserAndProductId_throwValidationException() throws Exception {
		final ValidationException validationException = new ValidationException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;
				result = validationException;
			}
		};

		productReviewService.getReviewsByUserAndProductId(11L, 10L);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetReviewsByUserAndProductId_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;

				ProductReviewApiValidator.validateField("userId", anyLong);
				minTimes = 1;

				productReviewManager.getReviewsByUserAndProductId(anyLong, anyLong);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewService.getReviewsByUserAndProductId(11L, 10L);
	}

	@Test
	public void testGetProductRating_notNull() throws Exception {
		final ProductRatingsDto productRatingsDto = new ProductRatingsDto();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;

				productReviewManager.getAverageWeightedRatingsByProductId(anyLong);
				minTimes = 1;
				result = productRatingsDto;
			}
		};

		ProductRatingsDto response = productReviewService.getProductRating(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ValidationException.class)
	public void testGetProductRating_throwValidationException() throws Exception {
		final ValidationException validationException = new ValidationException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;
				result = validationException;
			}
		};

		productReviewService.getProductRating(11L);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetProductRating_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;

				productReviewManager.getAverageWeightedRatingsByProductId(anyLong);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewService.getProductRating(11L);
	}

	@Test(expectedExceptions = Exception.class)
	public void testGetProductRating_throwException() throws ValidationException, ProductReviewServiceException {
		final Exception exception = new Exception();
		new StrictExpectations() {
			{
				ProductReviewApiValidator.validateField("productId", anyLong);
				minTimes = 1;

				productReviewManager.getAverageWeightedRatingsByProductId(anyLong);
				minTimes = 1;
				result = exception;
			}
		};

		productReviewService.getProductRating(11L);
	}

}
