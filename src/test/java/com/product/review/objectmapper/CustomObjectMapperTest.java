package com.product.review.objectmapper;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.product.review.entity.ProductReviewDto;
import com.product.review.jpa.entity.ProductReview;

import mockit.Injectable;
import mockit.StrictExpectations;
import mockit.Tested;

public class CustomObjectMapperTest {

	@Tested
	private CustomObjectMapper customObjectMapper;

	@Injectable
	private DozerBeanMapper dozerBeanMapper;

	@Test
	public void mapTest() {
		final ProductReview review = new ProductReview();
		review.setUserId(123l);
		review.setUserName("testUser");

		final ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");

		new StrictExpectations() {
			{
				dozerBeanMapper.map(review, ProductReviewDto.class);
				minTimes = 1;
				result = reviewdto;
			}
		};

		ProductReviewDto response = customObjectMapper.map(review, ProductReviewDto.class);
		Assert.assertEquals(response.getUserId().longValue(), 123l);
		Assert.assertEquals(response.getUserName(), "testUser");
	}

	@Test
	public void mapListTest() {
		final List<ProductReview> list1 = new ArrayList<ProductReview>();

		final ProductReview review = new ProductReview();
		review.setUserId(123l);
		review.setUserName("testUser");
		list1.add(review);

		final ProductReviewDto reviewdto = new ProductReviewDto();
		reviewdto.setUserId(123l);
		reviewdto.setUserName("testUser");

		new StrictExpectations() {
			{
				dozerBeanMapper.map(review, ProductReviewDto.class);
				minTimes = 1;
				result = reviewdto;
			}
		};

		List<ProductReviewDto> response = customObjectMapper.map(list1, ProductReviewDto.class);
		Assert.assertEquals(response.get(0).getUserId().longValue(), 123l);
		Assert.assertEquals(response.get(0).getUserName(), "testUser");
	}

}
