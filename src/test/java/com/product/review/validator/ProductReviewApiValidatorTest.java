package com.product.review.validator;

import org.slf4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.product.review.entity.ProductReviewDto;
import com.product.review.exception.ValidationException;

import mockit.Mocked;

public class ProductReviewApiValidatorTest {
	@Mocked(stubOutClassInitialization = true)
	private Logger logger;

	@Test
	public void validateReviewDtoTest() {
		ProductReviewDto reviewDto = new ProductReviewDto();
		reviewDto.setProductId(1123l);
		reviewDto.setUserId(234l);
		reviewDto.setUserName("testuser");
		reviewDto.setAttribute1Rating(1);
		reviewDto.setAttribute2Rating(2);
		reviewDto.setAttribute3Rating(3);
		reviewDto.setAttribute4Rating(4);
		reviewDto.setAttribute5Rating(5);
		reviewDto.setTitle("TestTitle");
		reviewDto.setComment("TestComment");

		try {
			ProductReviewApiValidator.validateReviewDto(reviewDto);
		} catch (ValidationException e) {
			Assert.fail("Some exception occurred");
		}
	}

	@Test(expectedExceptions = ValidationException.class)
	public void validateReviewDtoExceptionTest() throws ValidationException {
		ProductReviewDto reviewDto = null; // invalid object
		ProductReviewApiValidator.validateReviewDto(reviewDto);
	}

	@Test(expectedExceptions = ValidationException.class)
	public void validateReviewDtoException2Test() throws ValidationException {
		ProductReviewDto reviewDto = new ProductReviewDto();
		reviewDto.setProductId(1123l);
		reviewDto.setUserId(234l);
		reviewDto.setUserName("testuser");
//		reviewDto.setAttribute1Rating(null); // invalid rating
		reviewDto.setAttribute2Rating(2);
		reviewDto.setAttribute3Rating(3);
		reviewDto.setAttribute4Rating(4);
		reviewDto.setAttribute5Rating(5);
		reviewDto.setTitle("TestTitle");
		reviewDto.setComment("TestComment");
		ProductReviewApiValidator.validateReviewDto(reviewDto);
	}

	@Test(expectedExceptions = ValidationException.class)
	public void validateReviewDtoException3Test() throws ValidationException {
		ProductReviewDto reviewDto = new ProductReviewDto();
		reviewDto.setProductId(0l);// invalid product id
		reviewDto.setUserId(234l);
		reviewDto.setUserName("testuser");
		reviewDto.setAttribute1Rating(1);
		reviewDto.setAttribute2Rating(2);
		reviewDto.setAttribute3Rating(3);
		reviewDto.setAttribute4Rating(4);
		reviewDto.setAttribute5Rating(5);
		reviewDto.setTitle("TestTitle");
		reviewDto.setComment("TestComment");
		ProductReviewApiValidator.validateReviewDto(reviewDto);
	}

}
