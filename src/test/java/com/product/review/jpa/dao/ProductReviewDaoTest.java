package com.product.review.jpa.dao;

import com.product.review.exception.ProductReviewServiceException;
import com.product.review.jpa.dao.IProductReviewDao;
import com.product.review.jpa.dao.ProductReviewDao;
import com.product.review.jpa.entity.ProductRatings;
import com.product.review.jpa.entity.ProductReview;

import mockit.Injectable;
import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.Tested;
import org.slf4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

public class ProductReviewDaoTest {

	@Mocked(stubOutClassInitialization = true)
	public Logger logger;

	@Tested
	private ProductReviewDao productReviewDao;

	@Injectable
	private Query query;

	@Injectable
	private EntityManager entityManager;

	@Test
	public void testCreateReview_positiveCase() throws Exception {
		final ProductReview review = new ProductReview();
		new StrictExpectations() {
			{
				entityManager.persist(review);
				minTimes = 1;
			}
		};

		productReviewDao.createReview(review);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testCreateReview_throwProductReviewServiceException() throws Exception {
		final ProductReview review = new ProductReview();
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				entityManager.persist(review);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewDao.createReview(review);
	}

	@Test
	public void testGetReviewsByProductId_notNull() throws Exception {
		final List<ProductReview> list = new LinkedList<ProductReview>();
		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.FIND_BY_PRODUCT_ID_QUERY);
				minTimes = 1;
				result = query;

				query.setParameter("productId", 11L);
				minTimes = 1;

				query.getResultList();
				minTimes = 1;
				result = list;
			}
		};

		List<ProductReview> response = productReviewDao.getReviewsByProductId(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetReviewsByProductId_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.FIND_BY_PRODUCT_ID_QUERY);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewDao.getReviewsByProductId(11L);
	}

	@Test
	public void testGetAllRatingsForProduct_notNull() throws Exception {
		final List<Double> list = new LinkedList<Double>();
		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.RATINGS_BY_PRODUCT_ID_QUERY);
				minTimes = 1;
				result = query;

				query.setParameter("productId", 11L);
				minTimes = 1;

				query.getResultList();
				minTimes = 1;
				result = list;
			}
		};

		List<Double> response = productReviewDao.getAllRatingsForProduct(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetAllRatingsForProduct_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.RATINGS_BY_PRODUCT_ID_QUERY);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewDao.getAllRatingsForProduct(11L);
	}

	@Test
	public void testUpdateRatings_notNull() throws Exception {
		final ProductRatings rating = new ProductRatings();
		new StrictExpectations() {
			{
				entityManager.merge(rating);
			}
		};

		productReviewDao.updateRatings(rating);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testUpdateRatings_throwProductReviewServiceException() throws Exception {
		final ProductRatings rating = new ProductRatings();
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				entityManager.merge(rating);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewDao.updateRatings(rating);
	}

	@Test
	public void testGetAverageWeightedRatingsByProductId_notNull() throws Exception {
		final ProductRatings ratings = new ProductRatings();

		new StrictExpectations() {
			{
				entityManager.find(ProductRatings.class, 11L);
				minTimes = 1;
				result = ratings;
			}
		};

		ProductRatings response = productReviewDao.getAverageWeightedRatingsByProductId(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetAverageWeightedRatingsByProductId_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();
		new StrictExpectations() {
			{
				entityManager.find(ProductRatings.class, 11L);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewDao.getAverageWeightedRatingsByProductId(11L);
	}

	@Test
	public void testGetAllReviewsByUserId_notNull() throws Exception {
		final List<ProductReview> list = new LinkedList<ProductReview>();

		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.FIND_BY_USER_ID_QUERY);
				minTimes = 1;
				result = query;

				query.setParameter("userId", 11L);
				minTimes = 1;

				query.getResultList();
				minTimes = 1;
				result = list;
			}
		};

		List<ProductReview> response = productReviewDao.getAllReviewsByUserId(11L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetAllReviewsByUserId_throwProductReviewServiceException() throws Exception {
		final List<ProductReview> list = new LinkedList<ProductReview>();
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();

		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.FIND_BY_USER_ID_QUERY);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewDao.getAllReviewsByUserId(11L);
	}

	@Test
	public void testGetAllReviewsByUserIdForProduct_notNull() throws Exception {
		final List<ProductReview> list = new LinkedList<ProductReview>();

		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.FIND_BY_USER_AND_PRODUCT_ID_QUERY);
				minTimes = 1;
				result = query;

				query.setParameter("productId", 10L);
				minTimes = 1;
				query.setParameter("userId", 11L);
				minTimes = 1;

				query.getResultList();
				minTimes = 1;
				result = list;
			}
		};

		List<ProductReview> response = productReviewDao.getAllReviewsByUserIdForProduct(11L, 10L);
		Assert.assertNotNull(response);
	}

	@Test(expectedExceptions = ProductReviewServiceException.class)
	public void testGetAllReviewsByUserIdForProduct_throwProductReviewServiceException() throws Exception {
		final ProductReviewServiceException productReviewServiceException = new ProductReviewServiceException();

		new StrictExpectations() {
			{
				entityManager.createNamedQuery(IProductReviewDao.FIND_BY_USER_AND_PRODUCT_ID_QUERY);
				minTimes = 1;
				result = productReviewServiceException;
			}
		};

		productReviewDao.getAllReviewsByUserIdForProduct(11L, 10L);
	}
}
