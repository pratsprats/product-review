# README #

### What is this repository for? ###

* Quick summary - Api's exposed for any user to comment and rate any product.
* Version - v1

* Swagger link - http://ec2-18-220-92-232.us-east-2.compute.amazonaws.com:8080/productreview/ 
* Code repository - https://bitbucket.org/pratsprats/product-review
* Code coverage - 90%, 64 test cases (Screenshot checkin in - code-coverage-report.png)

* Database schema scripts - src/main/resources/ddl.sql
* Server - Tomcat installed on Amazon AWS EC2 (JDK8)
* Database - Amazon RDS MYSQL
* Rest services - Jboss Resteasy (http://resteasy.jboss.org/)
* JPA specification - Hibernate (using annotations)
* Cache layer - EhCache (Can be enabled and disabled from properties file - src/main/resources/app.properties) 
* Object Mapper - Dozer (http://dozer.sourceforge.net/)
* Application Framework - Spring
* Unit Tests and Mocking - TestNG and Jmockit

* For project checkout - "git clone https://pratsprats@bitbucket.org/pratsprats/product-review.git" 
* Build - Update the database configuration in src/main/resources/app.properties and then run "mvn clean package"
* Deployment - Copy productreview.war created in "target" folder to tomcat webapps and start.
* Code coverage report - Run "mvn clean cobertura:cobertura" and check report at location - target/site/cobertura/index.html


Assumptions - 
1. One user can comment and rate same product more than once. 
2. Each product can have a different rating attributes. 
3. Attribute Rating will be an integer between 1-5. 
4. User id and product id will be the the foreign key to User and Product table, which are not implemented here. 

Out of scope - 
1. Authentication and Authorization of api’s. 
2. Creation of support cases based on rating. 
3. Review updation. 
4. Pagination of api’s. 
5. Validation of product feedback text. 



